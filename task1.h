#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <bits/stdc++.h>

std::vector<std::string> Task_1(std::vector<std::string> a, std::vector<std::string> b);
std::vector<std::string> Task_1(std::vector<std::vector<std::string>> a);

// ----------------------------------------using templates ---------------------------------------- //
/**
 * @brief Sorting and removing dups from the finit vector
 * @tparam T comparable type
 * @param vec 
 */
template <typename T>
void remove_duplicates(std::vector<T> & vec)
{
    std::sort(vec.begin(), vec.end());
    vec.erase(std::unique(vec.begin(), vec.end()), vec.end());
}

/**
 * @brief Vector of vectors that become one without duplicates
 * @tparam T comparable type
 * @param vec 
 * @return std::vector<T> 
 */
template <typename T>
std::vector<T> Task_1(const std::vector<std::vector<T>> & vec)
{
    std::vector<T> result;
    for (int i = 0; i < vec.size(); i++)
    {
        result.insert(result.end(), vec[i].begin(), vec[i].end());
    }
    remove_duplicates(result);
    return result;
}

/**
 * @brief Any number of any type args added to vector that become one vector without duplicates
 * @tparam T comparable type
 * @tparam Args 
 * @param vec 
 * @param args 
 * @return std::vector<T> 
 */
template<typename T, typename... Args>
std::vector<T> Task_1(std::vector<T> vec, Args... args)
{
    std::vector<T> result;
    result.insert(result.end(), vec.begin(), vec.end());
    int concatenate[] = 
    {
        (
            result.insert(result.end(), args.begin(), args.end()
        ), 0)...
    };
    (void)concatenate;
    remove_duplicates(result);
    return result;
}

/**
 * @brief Any number of one type args added to vector that become one vector without duplicates
 * @tparam T comparable type
 * @param list 
 * @return std::vector<T> 
 */
template <typename T>
std::vector<T> Task_1(std::initializer_list<std::vector<T>> list)
{
    std::vector<T> result;
    for(auto elem : list)
    {
        result.insert(result.end(), elem.begin(), elem.end());
    }
    remove_duplicates(result);
    return result;
}


