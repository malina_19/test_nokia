#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <bits/stdc++.h>
#include "task1.h"

static std::vector<std::string> put_vector_to_vector(std::vector<std::string> vec, std::vector<std::string> & vec_of_strs);
static std::set<std::string> put_vector_to_set(std::vector<std::string> vec, std::set<std::string> & set_of_strs);
static void remove_duplicates(std::vector<std::string> & vec_of_strs);

/**
 * @brief Two vectors of strings that become one without duplicates
 * @param vec1 
 * @param vec2 
 * @return std::vector<std::string> 
 */
std::vector<std::string> Task_1(std::vector<std::string> vec1, std::vector<std::string> vec2)
{
    std::vector<std::string> vector;
    vector = put_vector_to_vector(vec1, vector);
    vector = put_vector_to_vector(vec2, vector);
    remove_duplicates(vector);
    return vector;
}

/**
 * @brief Vector of string vectors that become one without duplicates
 * @param vec 
 * @return std::vector<std::string> 
 */
std::vector<std::string> Task_1(std::vector<std::vector<std::string>> vec)
{
    std::vector<std::string> vector;
    for (int i=0; i < vec.size(); i++)
    {
        vector = put_vector_to_vector(vec[i], vector);
    }
    remove_duplicates(vector);
    std::cout << "The elements in vector are: " << std::endl;
    for(std::string & line : vector)
    {
        std::cout << line << std::endl;
    }
    return vector;
}

/**
 * @brief Vector vec added to vector vec_of_str
 * @param vec 
 * @param vec_of_strs 
 * @return std::vector<std::string> 
 */
static std::vector<std::string> put_vector_to_vector(std::vector<std::string> vec, std::vector<std::string> & vec_of_strs)
{
    for(int i=0; i < vec.size(); i++)
    {
        if(vec[i].size() > 0)
        {
            vec_of_strs.push_back(vec[i]);
        }
    }
    return vec_of_strs;
}

/**
 * @brief Vector vec added to set set_of_str
 * @param vec 
 * @param set_of_strs 
 * @return std::set<std::string> 
 */
static std::set<std::string> put_vector_to_set(std::vector<std::string> vec, std::set<std::string> & set_of_strs)
{
    for(int i=0; i < vec.size(); i++)
    {
        if(vec[i].size() > 0)
        {
            set_of_strs.insert(vec[i]);
        }
    }
    return set_of_strs;
}

/**
 * @brief Sorting and removing dups from the finit string vector
 * @param vec_of_strs 
 */
void remove_duplicates(std::vector<std::string> & vec_of_strs)
{
    std::sort(vec_of_strs.begin(), vec_of_strs.end());
    vec_of_strs.erase(std::unique(vec_of_strs.begin(), vec_of_strs.end()), vec_of_strs.end());
}
