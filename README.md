# Project Description
This is test task for Nokia. There is description in russian:
```
Задача 1
Даны N отсортированных списков. Написать функцию, которая объединяет списки в один отсортированный список уникальных значений.
Задача 2
Дана конечная последовательность ascii-символов. Требуется удалить из неё цепочки символов такие, что коды символов в них идут по возрастанию строго с шагом 1. В результате должна получится последовательность символов, в которой вообще нет таких цепочек (даже после удаления уже найденных).
Например, дана такая цепочка символов:
	wbtnaaqdefrluyzqa
Как видно, в ней есть цепочки символов, ascii-коды которых идут по возрастанию, а именно: def и yz, а после удаления возникает ещё цепочка qr. Таким образом, в ответе должно быть:
	wbtnaaluqa
```

# Build instructions
* Create folder "build" in source directory of project:
```
mkdir build
```
* Configure future build executing special make task in source directory of project:
```
make conf
```
* Build project:
```
make build
```
* To execute tests run foolowing:
```
make vtest
```
or
```
make test
```