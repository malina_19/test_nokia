#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <bits/stdc++.h>
#include "task2.h"

static void erase_sub_str(std::string & main_str, const std::string & to_erase);

/**
 * @brief ASCII string modyfied according to given condition
 * @param ASCII_string 
 * @return std::string 
 */
std::string Task_2(std::string ASCII_string)
{
    bool flag;
    while (flag != 1)
    {
        std::string to_erase = "";
        for(int i = 0; i < (ASCII_string.length() - 1); i++)
        {
            char symb;
            flag=0;
            symb = ASCII_string.at(i);
            if (int(symb)+1 == int(ASCII_string.at(i+1)))
            {
                to_erase.push_back(symb);
                to_erase.push_back(ASCII_string.at(i+1));
                flag = 0;
            }
            else
            {
                if (to_erase != "") 
                {
                    to_erase.erase(std::unique(to_erase.begin(), to_erase.end()), to_erase.end());
                    erase_sub_str(ASCII_string, to_erase);
                    to_erase = "";
                }
                else
                {
                    flag = 1;
                }
            }
        }
    }
    return ASCII_string;
    std::cout << "Modified string" << std::endl;
    std::cout << ASCII_string << std::endl; 
}

/**
 * @brief Erases one string from another
 * @param main_str 
 * @param to_erase 
 */
void erase_sub_str(std::string & main_str, const std::string & to_erase)
{
    // Search for the substring in string
    size_t pos = main_str.find(to_erase);
    if (pos != std::string::npos)
    {
        // If found then erase it from string
        main_str.erase(pos, to_erase.length());
    }
}