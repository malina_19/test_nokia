#include <gtest/gtest.h>
#include <vector>
#include <algorithm>
#include <array> 
#include <iostream>

#include "../task1.h"
#include "../task2.h"

TEST(task1_test1, ok)
{
  std::vector<std::vector<std::string>> str_vec_vec
  {
      {"a", "b", "d", "ef", "gk", "lh", "mn", "op", "qrst"}, 
      {"a", "b", "c", "def"}, 
      {"mn", "xyz" }
  }; 
  std::vector<std::vector<int>> int_vec_vec
  {
      {1,4,5,78,888,7654}, 
      {4,345,32222,456678, 33333333}, 
      {1,2,3,4,5,6,7,8,9,10}
  }; 

  std::vector<int> int_vec_0{1,4,5,78,888,7654};
  std::vector<int> int_vec_1{4,345,32222,456678, 33333333};
  std::vector<int> int_vec_2{1,2,3,4,5,6,7,8,9,10};

  auto result1 = Task_1<int>(int_vec_vec);
  auto result2 = Task_1<std::string>(str_vec_vec);
  auto result3 = Task_1<int>(int_vec_0, int_vec_1, int_vec_2);
  auto result4 = Task_1<int>({int_vec_0, int_vec_1, int_vec_2});
  // std::vector<std::string> result4 = {};
  auto is_vecs_empty = [=]()
  {
    if ( !result1.empty() && !result2.empty() && !result3.empty() && !result4.empty())
      { return true; }
    else
      { return false; }
  };
  ASSERT_TRUE(is_vecs_empty());
}

TEST(task1_test2, ok)
{
  std::vector<std::vector<std::string>> str_vec_vec
  {
      {"a", "b", "d", "ef", "gk", "lh", "mn", "op", "qrst"}, 
      {"a", "b", "c", "def"}, 
      {"mn", "xyz" }
  }; 
  auto result = Task_1<std::string>(str_vec_vec);
  // std::vector<std::string> result = {"a", "b", "d", "ef", "gk", "lh", "mn", "op", "qrst", "a", "b", "c", "def"};
  auto is_duplicates_in_vec = [=](std::vector<std::string> vec)
  {
    for(int i = 0; i < vec.size(); i++) 
    {  
      for(int j = 0; j < vec.size(); j++)
      {
        if (i != j)
        {
          if (vec[i] == vec[j])
          {
            return true;
          }
        }
      } 
    }
    return false;
  };
  ASSERT_FALSE(is_duplicates_in_vec(result));
}

TEST(task1_test3, ok)
{
  std::vector<std::vector<std::string>> str_vec_vec
  {
    {"a", "b", "d", "ef", "gk", "lh", "mn", "op", "qrst"}, 
    {"a", "b", "c", "def"}, 
    {"mn", "xyz"}
  }; 
  auto result = Task_1<std::string>(str_vec_vec);
  // std::vector<std::string> result = {"a", "b", "d", "ef", "gk", "lh", "mn", "op", "qrst", "a", "b", "c", "def"};
  ASSERT_TRUE(is_sorted(result.begin(), result.end()));
}

TEST(task1_test4, ok)
{
  std::vector<std::vector<std::string>> str_vec_vec
  {
    {"a", "b", "d", "ef", "gk", "lh", "mn", "op", "qrst"}, 
    {"a", "b", "c", "def"}, 
    {"mn", "xyz"}
  }; 
  auto result = Task_1<std::string>(str_vec_vec);
  auto has_every_vec = [=](std::vector<std::vector<std::string>> str_vec_of_vec, std::vector<std::string> no_dup_vec)
  {
    for(int i = 0; i < str_vec_of_vec.size(); i++) 
    {
      for(int j = 0; i < str_vec_of_vec[i].size(); j++) 
      {  
        if ( std::find(no_dup_vec.begin(), no_dup_vec.end(), str_vec_of_vec[i][j]) != no_dup_vec.end() )
        {
          return true;
        }
      }
    }
    return false;
  };
  ASSERT_TRUE(has_every_vec(str_vec_vec, result));
}

TEST(task2, ok)
{
  std::string str = "wbtnaaqdefrluyzqa";
  str = Task_2(str);
  auto is_ASCII_condition_true= [=](std::string str)
  {
    for(int i = 0; i < (str.length() - 1); i++) 
    {  
      if ( int(str.at(i))+1 == int(str.at(i+1)) )
      {
        return false;
      }
    }
    return true;
  };
  ASSERT_TRUE(is_ASCII_condition_true(str));
}



